r-cran-hunspell (3.0.5+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Tue, 28 Jan 2025 08:42:49 +0900

r-cran-hunspell (3.0.4+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Charles Plessy <plessy@debian.org>  Fri, 04 Oct 2024 08:43:10 +0900

r-cran-hunspell (3.0.3+dfsg2-2) unstable; urgency=medium

  * Packaging update
  * Standards-Version: 4.7.0 (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)

 -- Joost van Baal-Ilić <joostvb@debian.org>  Wed, 17 Apr 2024 09:07:37 +0000

r-cran-hunspell (3.0.3+dfsg2-1) unstable; urgency=medium

  [ Rene Engelhard ]
  * Shop shipping and using copied hunspell private headers
    - build-depend on libhunspell-private-dev
    - don't copy htypes.hxx and csutis.hxx but ln -sf them
    - use libhunspell-private-devs .shlibs to get a strict dependecy
      on libhunspell
    Closes: #1041276

  [ Andreas Tille ]
  * d/copyright: Drop unused license paragraphs
  * Drop inst/dict from upstream source since we will rather use the dictionary
    from the official Debian package
  * Use Debian packaged dictionaries (two tests have to be excluded)
    Closes: #1028130
  * d/rules: No need to fix any permissions any more
  * Fix clean target
    Closes: #1046117

 -- Andreas Tille <tille@debian.org>  Fri, 17 Nov 2023 09:48:53 +0100

r-cran-hunspell (3.0.3+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Set upstream metadata fields: Repository.

 -- Andreas Tille <tille@debian.org>  Fri, 13 Oct 2023 08:01:32 +0200

r-cran-hunspell (3.0.2+dfsg-3) unstable; urgency=medium

  * Rebuild against new version of libhunspell
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 28 Jun 2023 10:44:47 +0200

r-cran-hunspell (3.0.2+dfsg-2) experimental; urgency=medium

  * Enable building twice in a row (thanks for the patch to Rene Engelhard)
    Closes: #1028123
  * update copy of (private) htypes.hxx and csutil.hxx to fix build with
    hunspell 1.7.2. Bump build-dependency (thanks for the patch to Rene
    Engelhard)
    Closes: #1028124

 -- Andreas Tille <tille@debian.org>  Tue, 10 Jan 2023 14:31:02 +0100

r-cran-hunspell (3.0.2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 12 Sep 2022 12:37:44 +0200

r-cran-hunspell (3.0.1+dfsg-3) unstable; urgency=medium

  * Team Upload.
  * d/salsa-ci.yml: Disable reprotest
  * d/control: Fix minor mistake in synopsis
  * Add Forwarded field in patch

 -- Nilesh Patra <nilesh@debian.org>  Mon, 04 Oct 2021 16:59:21 +0530

r-cran-hunspell (3.0.1+dfsg-2) unstable; urgency=medium

  * Install authors file
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (3) (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 15 Sep 2021 12:08:45 +0200

r-cran-hunspell (3.0.1+dfsg-1) unstable; urgency=medium

  * Initial release (closes: #988841)

 -- Andreas Tille <tille@debian.org>  Thu, 20 May 2021 13:29:39 +0200
