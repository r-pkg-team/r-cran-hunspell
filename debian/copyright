Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: hunspell
Upstream-Contact: Jeroen Ooms <jeroen@berkeley.edu>
Source: https://cran.r-project.org/package=hunspell
Files-Excluded: */src/hunspell
                */inst/doc/*.html
                */inst/dict

Files: *
Copyright: 2012-2018 Jeroen Ooms,
 Authors of libhunspell (see AUTHORS file)
License: MIT

Files: src/parsers/*
Copyright: 2002-2017 Németh László
License: MPL-1.1 or GPL-2.0 or LGPL-2.1
 The contents of this file are subject to the Mozilla Public License Version
 1.1 (the "License"); you may not use this file except in compliance with
 the License. You may obtain a copy of the License at
 http://www.mozilla.org/MPL/
 .
 Software distributed under the License is distributed on an "AS IS" basis,
 WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 for the specific language governing rights and limitations under the
 License.
Comment: Hunspell is based on MySpell which is Copyright (C) 2002 Kevin Hendricks.
 Contributor(s): David Einstein, Davide Prina, Giuseppe Modugno,
 Gianluca Turconi, Simon Brouwer, Noll János, Bíró Árpád,
 Goldman Eleonóra, Sarlós Tamás, Bencsáth Boldizsár, Halácsy Péter,
 Dvornik László, Gefferth András, Nagy Viktor, Varga Dániel, Chris Halls,
 Rene Engelhard, Bram Moolenaar, Dafydd Jones, Harri Pitkänen
 .
 Alternatively, the contents of this file may be used under the terms of
 either the GNU General Public License Version 2 or later (the "GPL"), or
 the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 in which case the provisions of the GPL or the LGPL are applicable instead
 of those above. If you wish to allow use of your version of this file only
 under the terms of either the GPL or the LGPL, and not to allow others to
 use your version of this file under the terms of the MPL, indicate your
 decision by deleting the provisions above and replace them with the notice
 and other provisions required by the GPL or the LGPL. If you do not delete
 the provisions above, a recipient may use your version of this file under
 the terms of any one of the MPL, the GPL or the LGPL.

Files: debian/*
Copyright: 2019 Andreas Tille <tille@debian.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-2.0
 On Debian systems the full text of the GNU General Public License
 version 2.0 can be found at /usr/share/common-licenses/GPL-2.
